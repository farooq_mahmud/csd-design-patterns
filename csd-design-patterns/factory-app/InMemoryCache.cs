﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace factory_app
{ 
    public class InMemoryCache : ICache
    {
        public void Put(string key, string data)
        {
           Console.WriteLine($"{key}:{data} into InMemoryCache.");
        }

        public string Get(string key)
        {
            return "Data from InMemoryCache.";
        }
    }
}
