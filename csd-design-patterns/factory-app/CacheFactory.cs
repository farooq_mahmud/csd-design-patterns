﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace factory_app
{
    public class CacheFactory
    {
        public ICache CreateCache(CacheType cacheType)
        {
            switch (cacheType)
            {
                    case CacheType.Redis:
                        return new RedisCache();

                    case CacheType.InMemory:
                        return new InMemoryCache();

                    default:
                        return null;
            }
        }
    }
}
