﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace factory_app
{
    class Program
    {
        static void Main(string[] args)
        {
            var factory = new CacheFactory();
            var cache = factory.CreateCache(CacheType.Redis);

            cache.Put("redis", "data");
            Console.WriteLine(cache.Get("redis"));

            cache = factory.CreateCache(CacheType.InMemory);

            cache.Put("in-memory", "data");
            Console.WriteLine(cache.Get("in-memory"));

            Console.WriteLine("Press any key to end program.");
            Console.ReadLine();
        }
    }
}
