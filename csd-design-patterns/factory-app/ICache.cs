﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace factory_app
{
    public interface ICache
    {
        void Put(string key, string data);
        string Get(string key);
    }
}
