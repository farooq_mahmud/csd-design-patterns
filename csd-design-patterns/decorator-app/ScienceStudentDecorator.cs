﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace decorator_app
{
    public class ScienceStudentDecorator : StudentDecorator
    {
        public string Labs { get; set; }

        public ScienceStudentDecorator(StudentBase studentBase) : base(studentBase)
        {
        }

        public override string DisplayInformation()
        {
            return $"\nDEC >>> labs are {Labs}";
        }
    }
}
