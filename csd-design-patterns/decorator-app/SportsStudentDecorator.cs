﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace decorator_app
{
    public class SportsStudentDecorator : StudentDecorator
    {
        public string Sports { get; set; }

        public SportsStudentDecorator(StudentBase studentBase) : base(studentBase)
        {
        }

        public override string DisplayInformation()
        {
            return $"\n DEC >>> sports are {Sports}";
        }
    }
}
