﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace decorator_app
{
    public abstract class StudentBase
    {
        public string Name { get; set; }
        public int Age  { get; set; }
        public int Grade { get; set; }

        public virtual string DisplayInformation()
        {
            return $"{Name} who is {Age} years old is in grade {Grade}";
        }
    }
}
