﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace decorator_app
{
    class Program
    {
        static void Main(string[] args)
        {
            var student = new Student
                          {
                              Age = 12,
                              Grade = 7,
                              Name = "Bob"
                          };

            var decorators = new StudentDecorator[]
                             {
                                 new StudentDecorator(student),
                                 new ScienceStudentDecorator(student)
                                 {
                                     Labs = "Chemistry, Physics"
                                 },
                                 new SportsStudentDecorator(student)
                                 {
                                     Sports = "Basketball, Track"
                                 }
                             };

            var sb = new StringBuilder();

            foreach (var studentDecorator in decorators)
            {
                sb.AppendLine(studentDecorator.DisplayInformation());
            }

            Console.WriteLine(sb.ToString());
            Console.WriteLine("Press a key to end program.");
            Console.ReadLine();
        }
    }
}
