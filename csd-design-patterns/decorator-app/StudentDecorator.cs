﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace decorator_app
{
    public class StudentDecorator : Student
    {
        private readonly StudentBase _studentBase;

        public StudentDecorator(StudentBase studentBase)
        {
            _studentBase = studentBase;
        }

        public override string DisplayInformation()
        {
            return $"DEC >>> {_studentBase.DisplayInformation()}";
        }
    }
}
