﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace strategy_app
{
    public interface IBillingStrategy
    {
        decimal CalculateActualPrice(decimal originalPrice);
    }
}
