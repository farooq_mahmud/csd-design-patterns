﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace strategy_app
{
    class Program
    {
        static void Main(string[] args)
        {
            var customer = new Person(new CustomerBillingStrategy());
            customer.BuyItem(1M, 8);
            customer.BuyItem(2M, 1);
            Console.WriteLine(customer.GetBill());

            customer = new Person(new EmployeeBillingStrategy());
            customer.BuyItem(1M, 8);
            customer.BuyItem(2M, 1);
            Console.WriteLine(customer.GetBill());

            Console.WriteLine("Press any key to end program.");
            Console.ReadLine();
        }
    }
}
