﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace strategy_app
{
    public class Person
    {
        private readonly IBillingStrategy _billingStrategy;
        private readonly List<Tuple<decimal, int>> _items = new List<Tuple<decimal, int>>();

        public Person(IBillingStrategy billingStrategy)
        {
            _billingStrategy = billingStrategy;
        }

        public void BuyItem(decimal price, int quantity)
        {
            _items.Add(new Tuple<decimal, int>(price, quantity));
        }

        public string GetBill()
        {
            var total = _items.Sum(item => (item.Item1 * item.Item2));
            var acutalTotal = _billingStrategy.CalculateActualPrice(total);
            return $"Bill total is {acutalTotal}";
        }
    }
}
