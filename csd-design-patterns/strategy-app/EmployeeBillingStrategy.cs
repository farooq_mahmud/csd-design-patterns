﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace strategy_app
{ 
    public class EmployeeBillingStrategy : IBillingStrategy
    {
        public decimal CalculateActualPrice(decimal originalPrice)
        {
            return originalPrice - (originalPrice * 0.20M);
        }
    }
}
