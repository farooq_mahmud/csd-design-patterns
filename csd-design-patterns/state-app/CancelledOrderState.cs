﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace state_app
{
    public class CancelledOrderState : OrderState
    {
        public override string ToString()
        {
            return "Cancelled";
        }
    }
}
