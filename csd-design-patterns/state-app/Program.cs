﻿using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace state_app
{
    class Program
    {
        static void Main(string[] args)
        {
            var order = new Order
            {
                Id = 1,
                OrderDate = DateTime.UtcNow
            };

            PrintState(order);

            order.Ship();
            PrintState(order);

            order.Cancel();
            PrintState(order);

            order.Ship();
            PrintState(order);

            Console.WriteLine("Press a key to continue.");
            Console.ReadLine();

        }

        private static void PrintState(Order order)
        {
            Console.WriteLine($"Order state: {order.OrderState}");
        }
    }
}
