﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace state_app
{
    public abstract class OrderState : IOrderState
    {
        public virtual IOrderState New()
        {
            return this;
        }

        public virtual IOrderState Ship()
        {
            return this;
        }

        public virtual IOrderState Cancel()
        {
            return this;
        }

        public override string ToString()
        {
            return "NoChange";
        }
    }
}
