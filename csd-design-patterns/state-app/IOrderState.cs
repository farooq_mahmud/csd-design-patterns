﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace state_app
{
    public interface IOrderState
    {
        IOrderState New();
        IOrderState Ship();
        IOrderState Cancel();
    }
}
