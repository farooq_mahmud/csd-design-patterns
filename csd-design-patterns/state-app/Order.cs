﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace state_app
{
    public class Order
    {
        private IOrderState _orderState;

        public Order()
        {
            _orderState = new NewOrderState();
        }

        public int Id { get; set; }
        public DateTime OrderDate { get; set; }
        public IOrderState OrderState => _orderState;
        
        public void Cancel()
        {
            _orderState = _orderState.Cancel();
        }

        public void Ship()
        {
            _orderState = _orderState.Ship();
        }
    }
}
