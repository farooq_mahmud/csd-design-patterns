﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace state_app
{
    public class NewOrderState : OrderState
    {
        public override IOrderState Ship()
        {
            return new ShippedOrderState();
        }

        public override IOrderState Cancel()
        {
            return new CancelledOrderState();
        }

        public override string ToString()
        {
            return "New";
        }
    }
}
