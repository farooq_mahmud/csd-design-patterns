﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace state_app
{
    public class ShippedOrderState : OrderState
    {
        public override IOrderState Cancel()
        {
            return new CancelledOrderState();
        }

        public override string ToString()
        {
            return "Shipped";
        }
    }
}
