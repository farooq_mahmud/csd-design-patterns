﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace adapter_app
{
    public class Phone : ILightningPort
    {
        public void PushData(AudioData audioData)
        {
            Console.WriteLine($"Data type: {audioData.Type} First value: {audioData.Data[0]}");
        }
    }
}
