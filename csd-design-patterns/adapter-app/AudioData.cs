﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace adapter_app
{
    public class AudioData
    {
        public string Type { get; set; }
        public byte[] Data { get; set; }
    }
}
