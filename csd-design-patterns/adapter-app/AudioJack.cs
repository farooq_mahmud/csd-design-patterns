﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace adapter_app
{
    public class AudioJack : IAudioJack
    {
        public byte[] GetAudioStream()
        {
            return Encoding.UTF8.GetBytes("Audio stream...");
        }
    }
}
