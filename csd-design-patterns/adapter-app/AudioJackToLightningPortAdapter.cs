﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace adapter_app
{
    public class AudioJackToLightningPortAdapter
    {
        public AudioData GetLightiningDataFormat(IAudioJack audioJack)
        {
            var stream = audioJack.GetAudioStream();
            Console.WriteLine("Adapting audio jack data format.");

            return new AudioData
                   {
                       Data = stream,
                       Type = "ac3"
                   };
        }
    }
}
