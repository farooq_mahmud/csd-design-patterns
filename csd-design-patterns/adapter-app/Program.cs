﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace adapter_app
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var phone = new Phone();
            var adapter = new AudioJackToLightningPortAdapter();
            var audioJack = new AudioJack();
            var lightningData = adapter.GetLightiningDataFormat(audioJack);
            phone.PushData(lightningData);

            Console.WriteLine("Press a key to end program.");
            Console.ReadLine();
        }
    }
}
