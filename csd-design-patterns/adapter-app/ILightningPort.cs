﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace adapter_app
{
    public interface ILightningPort
    {
        void PushData(AudioData audioData);
    }
}
